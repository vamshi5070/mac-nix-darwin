-- installed ACLock spoon
-- installed Countdown spoon (A TIny countdown with visual indicator)
-- installed MUSIC APP MEDIA FIX
local alarmSound =  hs.sound.getByFile("/Users/vamshi/Downloads/clock-alarm-8761.mp3")

-- Function to play the alarm sound
function playAlarmSound()	 
  alarmSound:stop()
  alarmSound:play()
end

mouseCircle = nil
mouseCircleTimer = nil

function mouseHighlight()
    -- Delete an existing highlight if it exists
    if mouseCircle then
        mouseCircle:delete()
        if mouseCircleTimer then
            mouseCircleTimer:stop()
        end
    end
    -- Get the current co-ordinates of the mouse pointer
    mousepoint = hs.mouse.absolutePosition()
    -- Prepare a big red circle around the mouse pointer
    mouseCircle = hs.drawing.circle(hs.geometry.rect(mousepoint.x-40, mousepoint.y-40, 80, 80))
    mouseCircle:setStrokeColor({["red"]=1,["blue"]=0,["green"]=0,["alpha"]=1})
    mouseCircle:setFill(false)
    mouseCircle:setStrokeWidth(5)
    mouseCircle:show()

    -- Set a timer to delete the circle after 3 seconds
    mouseCircleTimer = hs.timer.doAfter(3, function()
      mouseCircle:delete()
      mouseCircle = nil
    end)
end

-- Schedule the alarm to play every 20 minutes
-- hs.timer.doEvery(120 , playAlarmSound)

-- hs.hotkey.bind({"cmd", "alt", "ctrl"}, "W", function()
  -- hs.alert.show("Take a break!!!!")
-- end)
hs.hotkey.bind({"cmd", "alt", "ctrl"}, "E", function()
  hs.notify.new({title="Hammerspoon", informativeText="Hello World"}):send()
end)

-- hs.hotkey.bind({"cmd", "alt", "ctrl"}, "T", function()
  -- hs.notify.new(hs.sound.play
-- end)
hs.loadSpoon("AClock")
spoon.AClock:init()
hs.hotkey.bind({"cmd", "alt", "ctrl"}, "C", function()
  spoon.AClock:toggleShowPersistent()
end)


function setAlarm()
  hs.alert.show("Take a break!!!!!!!!!!!!")
  hs.caffeinate.lockScreen()
  hs.timer.doAfter(1200, setAlarm) -- 1200 seconds = 20 minutes
end

function subAlarm()
  playAlarmSound()
  -- hs.alert.show("Take a break!!!!!!!!!!!!")
  spoon.AClock:toggleShowPersistent()
  mouseHighlight()
  hs.notify.new({title="Take a break", informativeText="A message from hammerspoon"}):send()
    -- hs.timer.doAfter(120, setAlarm) -- 1200 seconds = 20 minutes
end

hs.timer.doAfter(1200, setAlarm) -- Start the alarm 1 second after boot

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "R", function()
  hs.reload()
end)
hs.alert.show("Config loaded")

-- hs.hotkey.bind({"cmd","alt","shift"}, "D", mouseHighlight)

-- experiment
-- !!! moniter closes and sound when you press c-a-c q !!! 
hs.hotkey.bind({"cmd", "alt", "ctrl"}, "Q", function()
  hs.caffeinate.lockScreen()
end)

-- hs.loadSpoon("MusicAppMediaFix")

-- spoon.MusicAppMediaFix:start()