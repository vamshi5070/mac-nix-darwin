  (add-to-list 'load-path "~/mac-nix-darwin/emacs")
  (require 'org-keybindings)
  (require 'prog-keybindings)
  (require 'dired-keybindings)

(provide 'emux-keybindings)
