{
  description = "my minimal flake";
  inputs = {
    # Where we get most of our software. Giant mono repo with recipes
    # called derivations that say how to build software.
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable"; # nixos-22.11

    # Manages configs links things into your home directory
    home-manager.url = "github:nix-community/home-manager/master";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    # Controls system level software and settings including fonts
    darwin.url = "github:lnl7/nix-darwin";
    darwin.inputs.nixpkgs.follows = "nixpkgs";

    emacs.url = "github:cmacrae/emacs";

    # Tricked out nvim
    pwnvim.url = "github:zmre/pwnvim";
  };
  outputs = inputs: {
    darwinConfigurations.cosmos = inputs.darwin.lib.darwinSystem {
      system = "aarch64-darwin";
      pkgs = import inputs.nixpkgs {
        overlays = [inputs.emacs.overlay];

        system = "aarch64-darwin";
      };
      modules = [
        {
          nix.settings.substituters = ["https://cachix.org/api/v1/cache/emacs"];

          nix.settings.trusted-public-keys = [
            "emacs.cachix.org-1:b1SMJNLY/mZF6GxQE+eDBeps7WnkT0Po55TAyzwOxTY="
          ];
        }
        ({pkgs, ...}: {
          # here go the darwin preferences and config items
          programs.zsh.enable = true;
          environment.shells = [pkgs.bash pkgs.zsh];
          environment.loginShell = pkgs.zsh;
          environment.systemPackages = [pkgs.emacs pkgs.coreutils pkgs.nixfmt pkgs.texlive.combined.scheme-full];
          environment.systemPath = ["/opt/homebrew/bin"];
          environment.pathsToLink = ["/Applications"];
          nix.extraOptions = ''
            experimental-features = nix-command flakes
          '';
          system.keyboard.enableKeyMapping = true;
          system.keyboard.remapCapsLockToEscape = true;
          fonts.fontDir.enable = true; # DANGER
          fonts.fonts = [(pkgs.nerdfonts.override {fonts = ["Meslo" "CascadiaCode" "RobotoMono"];})];
          services.nix-daemon.enable = true;
          system.defaults.finder.AppleShowAllExtensions = true;
          system.defaults.finder._FXShowPosixPathInTitle = true;
          system.defaults.dock.autohide = true;
          system.defaults.NSGlobalDomain.AppleShowAllExtensions = true;
          system.defaults.NSGlobalDomain.InitialKeyRepeat = 14;
          system.defaults.NSGlobalDomain.KeyRepeat = 1;
          # backwards compat; don't change
          system.stateVersion = 4;
          homebrew = {
            enable = true;
            caskArgs.no_quarantine = true;
            global.brewfile = true;
            masApps = {};
            casks = ["raycast" "hammerspoon"];
            taps = ["fujiapple852/trippy"];
            brews = ["trippy" "yabai" "skhd"];
          };
        })

        inputs.home-manager.darwinModules.home-manager
        {
          home-manager = {
            useGlobalPkgs = true;
            useUserPackages = true;
            users.vamshi.imports = [
              ({
                pkgs,
                lib,
                ...
              }: {
                # Don't change this when you change package input. Leave it alone.
                home.stateVersion = "22.11";
                # specify my home-manager configs
                home.packages = [
                  pkgs.ripgrep
                  pkgs.rustfmt
                  pkgs.fd
                  pkgs.curl
                  pkgs.less
                  inputs.pwnvim.packages."aarch64-darwin".default
                ];
                home.sessionVariables = {
                  PAGER = "less";
                  CLICLOLOR = 1;
                  EDITOR = "nvim";
                };
                home.homeDirectory = lib.mkForce "/Users/vamshi";
                programs.bat.enable = true;
                programs.bat.config.theme = "TwoDark";
                programs.fzf.enable = true;
                programs.fzf.enableZshIntegration = true;
                programs.exa.enable = true;
                programs.git.enable = true;
                programs.zsh.enable = true;
                programs.zsh.enableCompletion = true;
                programs.zsh.enableAutosuggestions = true;
                programs.zsh.enableSyntaxHighlighting = true;
                programs.zsh.shellAliases = {ls = "ls --color=auto -F";};
                programs.starship.enable = true;
                programs.starship.enableZshIntegration = true;
                programs.alacritty = {
                  enable = true;
                  settings.font.normal.family = "MesloLGS Nerd Font Mono";
                  settings.font.size = 16;
                };
                home.file.".emacs.d/init.el".source = ./emacs/init.el;
                home.file.".hammerspoon/init.lua".source = ./hammerspoon/init.lua;
                home.file.".emacs.d/early-init.el".source =
                  ./emacs/early-init.el;

                home.file.".inputrc".text = ''
                  set show-all-if-ambiguous on
                  set completion-ignore-case on
                  set mark-directories on
                  set mark-symlinked-directories on
                  set match-hidden-files off
                  set visible-stats on
                  set keymap vi
                  set editing-mode vi-insert
                '';
              })
            ];
          };
        }
      ];
    };
  };
}
