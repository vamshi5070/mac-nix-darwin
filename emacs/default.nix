{ config, pkgs, ... }: {
  programs.emacs = {
    enable = true;
    extraPackages = epkgs:
      with epkgs;
      [
        # vterm
      ];
  };
  # home.packages = with pkgs; [cmake];

  home.file.".emacs.d/init.el".source = ./init.el;
  # home.file.".emacs.d/early-init.el".source = ./early-init.el;
}
