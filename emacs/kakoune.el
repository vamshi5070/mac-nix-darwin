
(defvar my-subword-forward-regexp "[[[:upper:]]*[[:digit:][:lower:]]+\\|[[:upper:]]+\\|[^[:word:][:space:]_\n]+\\|-|(|")

(defvar my-subword-backward-regexp "[[:space:][:word:]_\n][^\n[:space:][:word:]_]+\\|\\(\\W\\|[[:lower:]]\\)[[:upper:]]\\|\\W\\w+")
;; (defvar my-subword-backward-regexp "[^[:word:][:space:]_\n]+")
(defun my-subword-forward-internal () ""
       (interactive)  
       (let ((case-fold-search nil))
	 (re-search-forward my-subword-forward-regexp nil t))
       (goto-char (match-end 0)))

(defun my-subword-backward-internal () ""
       (interactive)
       (let ((case-fold-search nil))
	 (if (re-search-backward my-subword-backward-regexp nil "don't panic!")
	     (goto-char (1+ (match-beginning 0))))))

(setq subword-forward-regexp 'my-subword-forward-regexp)
(setq subword-backward-regexp 'my-subword-backward-regexp)
(setq subword-backward-function 'my-subword-backward-internal)
(setq subword-forward-function 'my-subword-forward-internal)
(subword-mode 1)
;; AAyn

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  kakoune  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (setq ryo-modal-cursor-color "#000000")
;;(global-set-key (kbd "<escape>")  'ryo-modal-mode)
(defun kakoune-a (count)
  "Select COUNT lines from the current line.
                     Note that kakoune's x does behaves exactly like this,
                     and I like this behavior better."
  (interactive "p")
  (deactivate-mark)
  (forward-char count)
  (ryo-modal-mode 0))

(defun kakoune-modal-a (count)
  "Select COUNT lines from the current line.
                     Note that kakoune's x does behaves exactly like this,
                     and I like this behavior better."
  (interactive "p")
  (deactivate-mark)
  (forward-char count)
  (modal-insert-mode))
;; (toggle-modal))

(defun kakoune-set-mark-here () "Set the mark at the location of the point."
       (interactive) (set-mark (point)))

(defun kakoune-set-mark-if-inactive () "Set the mark if it isn't active."
       (interactive)
       (unless (use-region-p) (set-mark (point))))

(defun kakoune-deactivate-mark ()
  "Deactivate the mark.
                     Deactivate the mark unless mark-region-mode is active."
  (interactive)
  ;;(unless rectangle-mark-mode (deactivate-mark)))
  (deactivate-mark))

(defun kakoune-O (count)
  "Open COUNT lines above the cursor and go into insert mode."
  (interactive "p")
  ;; (beginning-of-line)
  (open-line count))
;; (newline)
;; (forward-line -1)))

(defun kakoune-x (count)
  "Select COUNT lines from the current line.
                     Note that kakoune's x does behaves exactly like this,
                     and I like this behavior better."
  (interactive "p")
  (beginning-of-line count)
  (set-mark (point))
  (end-of-line count)
  (forward-char))

(defun kakoune-d (count)
  "Kill selected text or COUNT chars."
  (interactive "p")
  (if (use-region-p)
      (kill-region (region-beginning) (region-end))
    (delete-char count t)))

(defun kakoune-insert-mode () "Return to insert mode."
       (interactive)
       (ryo-modal-mode 0))
;;(defun kakoune-c ()
;; (interactive)
;; (kakoune-d )

(defun kakoune-c (count)
  "Kill selected text or COUNT chars."
  (interactive "p")
  (if (use-region-p)
      (kill-region (region-beginning) (region-end))
    (delete-char count t))
  (modal-insert-mode))
;; (toggle-modal)

(defun kakoune-h (count)
  (interactive "p")
  (deactivate-mark)
  ;; (kakoune-set-mark-if-inactive)
  (backward-char count)
  )
(defun kakoune-l (count)
  (interactive "p")
  ;; (kakoune-set-mark-if-inactive)
  (forward-char count)
  (deactivate-mark)
  )

(defun kakoune-j (count)
  (interactive "p")
  (deactivate-mark)
  (next-line count)
  )
(defun kakoune-k (count)
  (interactive "p")
  (deactivate-mark)
  (previous-line count)
  )
(defun kakoune-H (count)
  (interactive "p")
  (kakoune-set-mark-if-inactive)
  (backward-char count)
  )
(defun kakoune-L (count)
  (interactive "p")
  (kakoune-set-mark-if-inactive)
  (forward-char count)
  )

(defun kakoune-J (count)
  (interactive "p")
  (kakoune-set-mark-if-inactive)
  (next-line count)
  )
(defun kakoune-K (count)
  (interactive "p")
  (kakoune-set-mark-if-inactive)
  (previous-line count)
  )
(defun kakoune-w (count)
  (interactive "p")
  (kakoune-set-mark-here)
  (subword-forward count)
  )
(defun kakoune-e ()
  (interactive)
  (progn
    (kakoune-w)
    (kakoune-H)
    ))
(defun kakoune-f ()
  (interactive)
  (kakoune-set-mark-here)
  (kakoune-select-to-char))

(defun kakoune-W (count)
  (interactive "p")
  (kakoune-set-mark-if-inactive)
  (subword-forward count)
  )
(defun kakoune-B (count)
  (interactive "p")
  (kakoune-set-mark-if-inactive)
  (subword-backward count)
  )
(defun kakoune-b (count)
  (interactive "p")
  (kakoune-set-mark-here)
  (subword-backward count)
  )
(defun kakoune-exchange () "Return to insert mode."
       (interactive)
       (exchange-point-and-mark))

(defun kakoune-normal-mode () "Return to insert mode."
       (interactive)
       (ryo-modal-mode 1))

(defun ryo-after () "Enter normal mode"
       (interactive)
       (forward-char)
       (ryo-modal-mode 0))

(defun kakoune-X (count)
  "Extend COUNT lines from the current line."
  (interactive "p")
  (beginning-of-line)
  (unless (use-region-p) (set-mark (point)))
  (forward-line count))

(defun kakoune-M-x (count)
  "Extend COUNT lines from the current line."
  (interactive "p")
  (beginning-of-line)
  (unless (use-region-p) (set-mark (point)))
  (previous-line count))

(defun kakoune-gg (count)
  "Go to the beginning of the buffer or the COUNTth line."
  (interactive "p")
  (goto-char (point-min))
  (when count (forward-line (1- count))))

(defun kakoune-o (count)
  "Open COUNT lines under the cursor and go into insert mode."
  (interactive "p")
  (end-of-line)
  (dotimes (_ count)
    (electric-newline-and-maybe-indent))
  (ryo-modal-mode 0))

(defun kakoune-modal-o (count)
  "Open COUNT lines under the cursor and go into insert mode."
  (interactive "p")
  (end-of-line)
  (dotimes (_ count)
    (electric-newline-and-maybe-indent))
  (modal-insert-mode))
;; (toggle-modal))

(defun kakoune-modal-i ()
  (interactive )
  (deactivate-mark)
  (modal-insert-mode))

(defun kakoune-P (count)
  "Yank COUNT times after the point."
  (interactive "p")
  (forward-line)
  (dotimes (_ count) (save-excursion (yank))))

(defun kakoune-p (count)
  "Yank COUNT times after the point."
  (interactive "p")
  (dotimes (_ count) (save-excursion (yank))))

(defvar kakoune-last-t-or-f ?f
  "Using t or f command sets this variable.")

(defun kakoune-select-to-char (arg char)
  "Select up to, and including ARGth occurrence of CHAR.
                 Case is ignored if `case-fold-search' is non-nil in the current buffer.
                 Goes backward if ARG is negative; error if CHAR not found.
                 Ignores CHAR at point."
  (interactive "p\ncSelect to char: ")
  (setq kakoune-last-char-selected-to char)
  (setq kakoune-last-t-or-f ?f)
  (let ((direction (if (>= arg 0) 1 -1)))
    (forward-char direction)
    (unwind-protect
        (search-forward (char-to-string char) nil nil arg))
    (point)))

(defun kakoune-f (arg char)
  "Select up to, and including ARGth occurrence of CHAR.
                 Case is ignored if `case-fold-search' is non-nil in the current buffer.
                 Goes backward if ARG is negative; error if CHAR not found.
                 Ignores CHAR at point."
  (interactive "p\ncSelect to char: ")
  (kakoune-set-mark-here)
  (setq kakoune-last-char-selected-to char)
  (setq kakoune-last-t-or-f ?f)
  (let ((direction (if (>= arg 0) 1 -1)))
    (forward-char direction)
    (unwind-protect
        (search-forward (char-to-string char) nil nil arg))
    (point)))

(defun kakoune-F (arg char)
  "Select up to, and including ARGth occurrence of CHAR.
                 Case is ignored if `case-fold-search' is non-nil in the current buffer.
                 Goes backward if ARG is negative; error if CHAR not found.
                 Ignores CHAR at point."
  (interactive "p\ncSelect to char: ")
  (kakoune-set-mark-if-inactive)
  (setq kakoune-last-char-selected-to char)
  (setq kakoune-last-t-or-f ?f)
  (let ((direction (if (>= arg 0) 1 -1)))
    (forward-char direction)
    (unwind-protect
        (search-forward (char-to-string char) nil nil arg))
    (point)))
(defun kakoune-select-up-to-char (arg char)
  "Select up to, but not including ARGth occurrence of CHAR.
                 Case is ignored if `case-fold-search' is non-nil in the current buffer.
                 Goes backward if ARG is negative; error if CHAR not found.
                 Ignores CHAR at point."
  (interactive "p\ncSelect up to char: ")
  (setq kakoune-last-char-selected-to char)
  (setq kakoune-last-t-or-f ?t)
  (let ((direction (if (>= arg 0) 1 -1)))
    (forward-char direction)
    (unwind-protect
        (search-forward (char-to-string char) nil nil arg)
      (backward-char direction))
    (point)))

(defun kakoune-t (arg char)
  "Select up to, but not including ARGth occurrence of CHAR.
                 Case is ignored if `case-fold-search' is non-nil in the current buffer.
                 Goes backward if ARG is negative; error if CHAR not found.
                 Ignores CHAR at point."
  (interactive "p\ncSelect up to char: ")
  (kakoune-set-mark-here)
  (setq kakoune-last-char-selected-to char)
  (setq kakoune-last-t-or-f ?t)
  (let ((direction (if (>= arg 0) 1 -1)))
    (forward-char direction)
    (unwind-protect
        (search-forward (char-to-string char) nil nil arg)
      (backward-char direction))
    (point)))


(defun kakoune-T (arg char)
  "Select up to, but not including ARGth occurrence of CHAR.
                 Case is ignored if `case-fold-search' is non-nil in the current buffer.
                 Goes backward if ARG is negative; error if CHAR not found.
                 Ignores CHAR at point."
  (interactive "p\ncSelect up to char: ")
  (kakoune-set-mark-if-inactive)
  (setq kakoune-last-char-selected-to char)
  (setq kakoune-last-t-or-f ?t)
  (let ((direction (if (>= arg 0) 1 -1)))
    (forward-char direction)
    (unwind-protect
        (search-forward (char-to-string char) nil nil arg)
      (backward-char direction))
    (point)))
(defun kakoune-word-backward ()
  (interactive)
  ;; (progn (forward-char)
  ;;     (forward-word))
  (subward-backward
   ))

(defun kakoune-word-forward ()
  (interactive)
  ;; (progn (forward-char)
  ;;     (forward-word))
  (forward-word
   ))

(defun kakoune-forward-char ()
  (interactive)
  (forward-char))

(defun kakoune-wrong-key ()
  (interactive)
  (message "pressed wrong key"))

(defun kakoune-A (count)
  "Yank COUNT times after the point."
  (interactive "p")
  (end-of-line)
  (ryo-modal-mode 0))

(defun kakoune-modal-A (count)
  "Yank COUNT times after the point."
  (interactive "p")
  (end-of-line)
  (modal-insert-mode))
;; (toggle-modal))

(defun kakoune-downcase ()
  "Downcase region."
  (interactive)
  (if (use-region-p)
      (downcase-region (region-beginning) (region-end))
    (downcase-region (point) (+ 1 (point)))))

(defun kakoune-upcase ()
  "Upcase region."
  (interactive)
  (if (use-region-p)
      (upcase-region (region-beginning) (region-end))
    (upcase-region (point) (1+ (point)))))

(defun kakoune-replace-char (char)
  "Replace selection with CHAR."
  (interactive "cReplace with char: ")
  (mc/execute-command-for-all-cursors
   (lambda () (interactive)
     (if (use-region-p)
	 (progn (let ((region-size (- (region-end) (region-beginning))))
		  (delete-region (region-beginning) (region-end))
		  (mc/save-excursion
                   (insert-char char region-size t))))
       (progn (delete-region (point) (1+ (point)))
              (mc/save-excursion
               (insert-char char)))))))

(defun kakoune-replace-selection ()
  "Replace selection with killed text."
  (interactive)
  (if (use-region-p)
      (progn (delete-region (region-beginning) (region-end))
             (yank))
    (progn (delete-region (point) (1+ (point)))
           (yank))))

(defun kakoune-insert-line-below (count)
  "Insert COUNT empty lines below the current line."
  (interactive "p")
  (save-excursion
    (end-of-line)
    (open-line count)))

(defun kakoune-insert-line-above (count)
  "Insert COUNT empty lines above the current line."
  (interactive "p")
  (save-excursion
    (end-of-line 0)
    (open-line count)))

(defun kakoune-paste-above (count)
  "Paste (yank) COUNT times above the current line."
  (interactive "p")
  (save-excursion
    (dotimes (_ count) (end-of-line 0)
             (newline)
             (yank))))

(defun kakoune-paste-below (count)
  "Paste (yank) COUNT times below the current line."
  (interactive "p")
  (save-excursion
    (dotimes (_ count) (end-of-line)
             (newline)
             (yank))))

(defun my-save-buffer ()
  (interactive)
  (let ((input (read-string "Enter command: " ":")))
    (if (string-equal input ":w")
	(progn (save-buffer)
	       (message "Buffer saved."))
      (message "Improper input")
      )))

(defun search-C-s ()
  (interactive)
  (or (consult-line) (isearch-forward)))

(provide 'kakoune)
