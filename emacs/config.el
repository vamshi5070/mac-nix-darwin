(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)
(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))
(setq create-lockfiles nil)

(load-theme 'modus-vivendi t)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

(straight-use-package '( vertico :files (:defaults "extensions/*")
             :includes (vertico-buffer
                    vertico-directory
                    vertico-flat
                    vertico-indexed
                    vertico-mouse
                    vertico-quick
                    vertico-repeat
                    vertico-reverse)))
  ;; (straight-use-package 'vertico)
  (straight-use-package 'orderless)
    ;; (straight-use-package 'vertico-directory)
  (straight-use-package 'consult)
(setq consult-preview-key nil) ; No live preview
    (vertico-mode t)
    (vertico-indexed-mode t)
           (setq vertico-count 17
           vertico-resize nil
             vertico-cycle t
             vertico-count-format nil ; No prefix with number of entries
             completion-in-region-function
       (lambda (&rest args)
         (apply (if vertico-mode
                #'consult-completion-in-region
              #'completion--in-region)
            args)))

       (define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
       (define-key vertico-map (kbd "C-j") #'vertico-next)
       (define-key vertico-map (kbd "C-k") #'vertico-previous)
       (define-key vertico-map (kbd "C-'") #'vertico-quick-insert)
       (define-key vertico-map (kbd "M-RET") #'minibuffer-force-complete-and-exit)
     ;; (define-key vertico-map (kbd "<backspace>") #'vertico-directory-delete-char)
  ;; (require 'vertico-directory)

    ;; Configure directory extension.
    (use-package vertico-directory
  :straight nil
  :after vertico
  :ensure nil
  ;; More convenient directory navigation commands
  :bind (:map vertico-map
          ("RET" . vertico-directory-enter)
          ("DEL" . vertico-directory-delete-char)
          ("M-DEL" . vertico-directory-delete-word))
  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))
      (setq completion-styles '(orderless)
            completion-category-defaults nil
            completion-category-overrides '((file (styles partial-completion))))

       (global-set-key (kbd "C-x b") 'consult-buffer)
       (global-set-key (kbd "C-x C-b") 'consult-buffer)
    ;; (vertico-posframe-mode 1)
    ;; (mini-frame-mode 1)	;; 
    ;; (minibuffer-header-mode 1)
      ;; (require 'io-bling)
     ;; (require 'emux-minibuffer)


    ;; (load-library 'ob-haskell nil)

(global-set-key (kbd "C-x C-o") 'other-window)

(straight-use-package 'nix-mode)

(straight-use-package 'haskell-mode)

(straight-use-package 'elm-mode)

(straight-use-package 'pulsar)
(customize-set-variable
 'pulsar-pulse-functions
 '(other-window
   xah-next-window-or-frame)
 )
(setq pulsar-face 'pulsar-magenta)
(setq pulsar-delay 0.055)
;; (define-key global-map (kbd  
(pulsar-global-mode t)
(global-hl-line-mode t)

(use-package marginalia
  :ensure t
  :config
  (marginalia-mode))

(use-package embark
  :ensure t

  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :init

  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  :config

  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :ensure t ; only need to install it, embark loads it after consult if found
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(require 'dired)

(put 'dired-find-alternate-file 'disabled nil)
;; Old alternative for dired-kill-when-opening-new-dired-buffer option.
(add-hook 'dired-mode-hook 'dired-hide-details-mode)
(add-hook 'dired-mode-hook 'dired-omit-mode)
(setq-default dired-recursive-copies 'top   ;; Always ask recursive copy
	      dired-recursive-deletes 'top  ;; Always ask recursive delete
	      dired-dwim-target t	    ;; Copy in split mode with p
	      dired-auto-revert-buffer t
	      dired-listing-switches "-alh -agho --group-directories-first"
	      dired-kill-when-opening-new-dired-buffer t ;; only works for emacs > 28
	      dired-isearch-filenames 'dwim ;;)
	      dired-omit-files "^\\.[^.].*"
	      dired-omit-verbose nil
	      dired-hide-details-hide-symlink-targets nil
	      delete-by-moving-to-trash t
	      )
;; (emux/leader-key-def 
;; "fd" 'dired-jump)

(defun emux/window-front ()
  (interactive)
  (other-window 1))

(defun emux/window-back ()
  (interactive)
  (other-window -1))

(defun emux/horizontal-split ()
  (interactive )
  (split-window-below)
  (emux/window-front))
;; (other-window 1))

(defun emux/vertical-split ()
  (interactive )
  (split-window-right)
  (emux/window-front))
;;  (other-window 1))

(defun emux/up-window ()
  (interactive )
  (emux/window-back))
;;  (other-window -1))

(defun emux/down-window ()
  (interactive )
  (emux/window-front))
(defun emux/swap-with-front-window ()
  (interactive)
  (save-excursion
    (let ((cur-buff (current-buffer)))
      (emux/window-front)
      (let ((other-buff (current-buffer)))
	(switch-to-buffer cur-buff)
	(emux/window-back)
	(switch-to-buffer other-buff)))))
(defun emux/swap-with-back-window ()
  (interactive)
  (save-excursion
    (let ((cur-buff (current-buffer)))
      (emux/window-back)
      (let ((other-buff (current-buffer)))
	(switch-to-buffer cur-buff)
	(emux/window-front)
	(switch-to-buffer other-buff)))))
;;	   (other-window 1))

(defun my-ssh ()
  (interactive)
  (find-file "/ssh:vamshi@192.168.64.3:/home/vamshi/")
  ;; (insert "/ssh:vamshi@192.168.64.2:/")
  )

(savehist-mode t)
 (save-place-mode t)
(setq save-place-ignore-files-regexp
       (replace-regexp-in-string "\\\\)\\$" "\\|^/tmp/.+\\)$"
				 save-place-ignore-files-regexp t t))

(defun emux/window-front ()
  (interactive)
  (other-window 1))

(defun emux/window-back ()
  (interactive)
  (other-window -1))

(defun emux/horizontal-split ()
  (interactive )
  (split-window-below)
  (emux/window-front))
;; (other-window 1))

(defun emux/vertical-split ()
  (interactive )
  (split-window-right)
  (emux/window-front))
;;  (other-window 1))

(defun emux/up-window ()
  (interactive )
  (emux/window-back))
;;  (other-window -1))

(defun emux/down-window ()
  (interactive )
  (emux/window-front))
(defun emux/swap-with-front-window ()
  (interactive)
  (save-excursion
    (let ((cur-buff (current-buffer)))
      (emux/window-front)
      (let ((other-buff (current-buffer)))
	(switch-to-buffer cur-buff)
	(emux/window-back)
	(switch-to-buffer other-buff)))))
(defun emux/swap-with-back-window ()
  (interactive)
  (save-excursion
    (let ((cur-buff (current-buffer)))
      (emux/window-back)
      (let ((other-buff (current-buffer)))
	(switch-to-buffer cur-buff)
	(emux/window-front)
	(switch-to-buffer other-buff)))))
;;	   (other-window 1))

(use-package evil
:straight t
  :ensure t
  :init
  (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1))

(use-package evil-collection
:straight t
  :after evil
  :ensure t
  :config
  (evil-collection-init))
(evil-global-set-key 'normal (kbd "SPC b b") 'consult-buffer)
(evil-global-set-key 'normal (kbd "SPC s s") 'consult-line)
(evil-global-set-key 'normal (kbd "SPC f s") 'save-buffer)
(evil-global-set-key 'normal (kbd "SPC f f") 'find-file)
(evil-global-set-key 'normal  (kbd "SPC SPC") 'execute-extended-command)
(evil-global-set-key 'visual (kbd "SPC SPC") 'execute-extended-command)


(evil-global-set-key 'normal (kbd "SPC w w") 'other-window)
(evil-global-set-key 'normal (kbd "SPC w s") 'emux/horizontal-split)
(evil-global-set-key 'normal (kbd "SPC w v") 'emux/vertical-split)
(evil-global-set-key 'normal (kbd "SPC w d") 'delete-window)


(evil-global-set-key 'normal (kbd "SPC w j") 'emux/window-front)
(evil-global-set-key 'normal (kbd "SPC w k") 'emux/window-back)

(evil-global-set-key 'normal (kbd "SPC w a") 'delete-other-windows)

(column-number-mode 1)
(global-display-line-numbers-mode 0)
(setq display-line-numbers-type 'visual)
(dolist (mode '( org-mode-hook
		   prog-mode-hook
		   dired-mode-hook
		   ;; term-mode-hook
		   ;; vterm-mode-hook
		   ;; shell-mode-hook
		   ;; dired-mode-hook
		   ;; eshell-mode-hook
		   ))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))

(setq 
 ;; org-ellipsis "▼  " ;;⤵ 
 ;; org-startup-indented t
 org-src-tab-acts-natively t
 org-hide-emphasis-markers t
 org-fontify-done-headline t
 org-hide-leading-stars t
 org-pretty-entities t
 org-odd-levels-only t
 ) 
;;  (add-hook 'org-mode-hook #'org-shifttab)
;; (add-hook 'org-mode-hook #'variable-pitch-mode)
;; (setq org-bullets-bullet-list '(
;; (add-hook 'org-mode-hook (lambda () (local-set-key (kbd "
(setq org-src-fontify-natively t
      org-startup-folded t
      org-edit-src-content-indentation 0)

(set-face-attribute 'default nil
                    :height 190)
(set-face-attribute 'variable-pitch nil
                    :height 170)
(set-face-attribute 'fixed-pitch nil
                    :height 200)

(use-package magit
 :straight t)

(use-package eglot
 :straight t)

(straight-use-package '(tsx-mode :type git :host github :repo "orzechowskid/tsx-mode.el" :branch "emacs29"))

(use-package svelte-mode
  :straight t)

(use-package prettier-js
  :straight t)
(add-hook 'svelte-mode-hook 'prettier-mode)

(straight-use-package '(treesit-auto :type git :host github :repo "renzmann/treesit-auto" :branch "emacs29"))
;; (use-package treesit-auto
  ;; :straight t
  ;; :config
  ;; (global-treesit-auto-mode))
