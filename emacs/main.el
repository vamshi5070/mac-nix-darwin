(set-face-attribute 'default nil :font "SF Mono"
		  :height 190)

;; (load-theme 'modus-vivendi t)

(setq user-full-name "vamshi")

;; (global-auto-revert-mode 1)
;;(auto-revert-tail-mode)
 ;; (global-revert-tail-mode)

(electric-pair-mode t)

(savehist-mode t)
(save-place-mode t)
(setq save-place-ignore-files-regexp
      (replace-regexp-in-string "\\\\)\\$" "\\|^/tmp/.+\\)$"
				save-place-ignore-files-regexp t t))

(recentf-mode 1)

(column-number-mode 1)
(global-display-line-numbers-mode 0)
(setq display-line-numbers-type 'visual)
(dolist (mode '( org-mode-hook
		 prog-mode-hook
		 dired-mode-hook

		 ;; term-mode-hook
		 ;; vterm-mode-hook
		 ;; shell-mode-hook
		 ;; dired-mode-hook
		 ;; eshell-mode-hook
		 ))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))

(display-time)

(setq org-startup-folded t)
(setq org-startup-with-inline-images t)

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)
(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))
(setq create-lockfiles nil)

(setq org-agenda-files '("~/tasks/timetable.org")
       org-agenda-span  'week
       org-todo-keywords '((sequence "NEXT(n)" "TODO(t)" "WAITING(w)" "SOMEDAY(s)" "|" "DONE(d)" "CANCELLED (c)")) 
       org-log-done 'time
       org-log-done 'note
       )

(global-set-key (kbd "C-c a") 'org-agenda)
(setq org-log-into-drawer '("LOGBOOK"))



(require 'dired)

(put 'dired-find-alternate-file 'disabled nil)
;; Old alternative for dired-kill-when-opening-new-dired-buffer option.
(add-hook 'dired-mode-hook 'dired-hide-details-mode)
(add-hook 'dired-mode-hook 'dired-omit-mode)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

(straight-use-package 'catppuccin-theme)
(straight-use-package 'all-the-icons)
(setq catppuccin-flavour 'frappe)
(use-package lambda-themes
  :straight (:type git :host github :repo "lambda-emacs/lambda-themes") 
  :custom
  (lambda-themes-set-italic-comments t)
  (lambda-themes-set-italic-keywords t)
  (lambda-themes-set-variable-pitch t) 
  :config
  ;; load preferred theme 
  (load-theme 'lambda-light t))
;;(load-theme 'catppuccin t)

(use-package lambda-line
:straight (:type git :host github :repo "lambda-emacs/lambda-line") 
:custom
(lambda-line-icon-time t) ;; requires ClockFace font (see below)
(lambda-line-clockface-update-fontset "ClockFaceRect") ;; set clock icon
(lambda-line-position 'top) ;; Set position of status-line 
(lambda-line-abbrev t) ;; abbreviate major modes
(lambda-line-hspace "  ")  ;; add some cushion
(lambda-line-prefix t) ;; use a prefix symbol
(lambda-line-prefix-padding nil) ;; no extra space for prefix 
(lambda-line-status-invert nil)  ;; no invert colors
(lambda-line-gui-ro-symbol  " ⨂") ;; symbols
(lambda-line-gui-mod-symbol " ⬤") 
(lambda-line-gui-rw-symbol  " ◯") 
(lambda-line-space-top +.50)  ;; padding on top and bottom of line
(lambda-line-space-bottom -.50)
(lambda-line-symbol-position 0.1) ;; adjust the vertical placement of symbol
:config
;; activate lambda-line 
(lambda-line-mode) 
;; set divider line in footer
(when (eq lambda-line-position 'top)
  (setq-default mode-line-format (list "%_"))
  (setq mode-line-format (list "%_"))))

;; (load-theme 'adwaita t)
 (use-package tron-legacy-theme
   :straight t
   :config
   (setq tron-legacy-theme-vivid-cursor t))
 ;; (load-theme 'tron-legacy t))

 (use-package tok-theme
   :straight t)
 ;; :config
 ;; (load-theme 'tok t))

 (use-package timu-spacegrey-theme
   :straight t)
 (customize-set-variable 'timu-spacegrey-flavour "light")
;; (load-theme 'timu-spacegrey t)

 (use-package timu-rouge-theme
   :straight t
   :ensure t)
   ;; :config
   ;; (load-theme 'timu-rouge t))

(straight-use-package '(nano-theme :type git :host github
				    :repo "rougier/nano-theme"))
;; (load-theme 'nano-dark t)
 ;;  (nano-dark)

;;    (straight-use-package '(nano-theme :type git :host github
  ;;				     :repo "rougier/nano-model"))
  (straight-use-package 'nano-modeline)
;; (nano-modeline-text-mode t)

(straight-use-package 'vterm)

(straight-use-package 'nix-mode)

(straight-use-package 'go-mode)

(straight-use-package 'rust-mode)

(defun insert-main-rust ()
  (interactive)
  (insert "fn main() {
	       println!(\"enter here\");
	  }"))

(straight-use-package 'haskell-mode)

(straight-use-package '( vertico :files (:defaults "extensions/*")
			 :includes (vertico-buffer
				    vertico-directory
				    vertico-flat
				    vertico-indexed
				    vertico-mouse
				    vertico-quick
				    vertico-repeat
				    vertico-reverse)))
(straight-use-package 'vertico)
(require 'vertico)
(define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
(define-key vertico-map (kbd "C-j") #'vertico-next)
(define-key vertico-map (kbd "C-k") #'vertico-previous)
(define-key vertico-map (kbd "C-'") #'vertico-quick-insert)
(vertico-mode 1)

;; Configure directory extension.
(use-package vertico-directory
  :straight nil
  :after vertico
  :ensure nil
  ;; More convenient directory navigation commands
  :bind (:map vertico-map
	      ("RET" . vertico-directory-enter)
	      ("DEL" . vertico-directory-delete-char)
	      ("M-DEL" . vertico-directory-delete-word))
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

(straight-use-package 'consult)
 (setq vertico-count 12
         vertico-resize nil
           vertico-cycle t
           vertico-count-format nil ; No prefix with number of entries
           completion-in-region-function
     (lambda (&rest args)
       (apply (if vertico-mode
              #'consult-completion-in-region
            #'completion--in-region)
          args)))

(straight-use-package 'orderless)
(setq completion-styles '(orderless)
      completion-category-defaults nil
      completion-category-overrides '((file (styles partial-completion))))

(straight-use-package 'magit)

(defun config-open ()
  (interactive)
  (find-file "~/mac-nix-darwin/emacs/main.org")
  )

;; ref:- C-x C-f /ssh:you@remotehost|sudo:remotehost:/path/to/file RET
(defun connect-remote ()
  (interactive)
  (dired "/ssh:vamshi@192.168.64.3:/home/vamshi"))
(defun insert-ssh ()
  (interactive)
  (insert "ssh vamshi@192.168.64.3"))

(defun emux/window-front ()
  (interactive)
  (other-window 1))

(defun emux/window-back ()
  (interactive)
  (other-window -1))

(defun emux/horizontal-split ()
  (interactive )
  (split-window-below)
  (emux/window-front))
;; (other-window 1))

(defun emux/vertical-split ()
  (interactive )
  (split-window-right)
  (emux/window-front))
;;  (other-window 1))

(defun emux/up-window ()
  (interactive )
  (emux/window-back))
;;  (other-window -1))

(defun emux/down-window ()
  (interactive )
  (emux/window-front))
(defun emux/swap-with-front-window ()
  (interactive)
  (save-excursion
    (let ((cur-buff (current-buffer)))
      (emux/window-front)
      (let ((other-buff (current-buffer)))
	(switch-to-buffer cur-buff)
	(emux/window-back)
	(switch-to-buffer other-buff)))))
(defun emux/swap-with-back-window ()
  (interactive)
  (save-excursion
    (let ((cur-buff (current-buffer)))
      (emux/window-back)
      (let ((other-buff (current-buffer)))
	(switch-to-buffer cur-buff)
	(emux/window-front)
	(switch-to-buffer other-buff)))))
;;	   (other-window 1))

(require 'window)
 (setq display-buffer-alist
	`(
	  ("\\*vterm*"
	   (display-buffer-in-side-window)
	   (dedicated . t)
	   (window-height . 0.23)
	   (side . bottom)
	   (slot . 0)
	   )
	  ("\\*shell*"
	   (display-buffer-in-side-window)
	   (dedicated . t)
	   (window-height . 0.23)
	   (side . bottom)
	   (slot . -1)
	   )
	  ("\\`\\*Async Shell Command\\*\\'"
	   (display-buffer-in-side-window)
	   (dedicated . t)
	   (window-height . 0.23)
	   (side . right)
	   (slot . 0)
	   )
	  ("\\*scratch*"
	   (display-buffer-in-side-window)
	   (dedicated . t)
	   (window-height . 0.23)
	   (side . top)
	   (slot . 0)
	   )
	  ;; ("*"
	  ;;  (display-buffer-in-side-window)
	  ;;  (mode . (dired-mode))
	  ;;  (dedicated . t)
	  ;;  (window-height . 0.23)
	  ;;  (side . bottom)
	  ;;  (slot . 0)
	  ;;  )
	  ("*Help*"
	   (display-buffer-in-side-window)
	   (dedicated . t)
	   (window-height . 0.23)
	   (side . bottom) 
	   (slot . 1)
	   )
	  ("*Occur*"
	   (display-buffer-in-side-window)
	   (dedicated . t)
	   (window-height . 0.23)
	   (side . top) 
	   (slot . -1)
	   )
	  ("*grep*"
	   (display-buffer-in-side-window)
	   (dedicated . t)
	   (window-height . 0.23)
	   (side . top) 
	   (slot . 1)
	   )
	  ("\\*haskell*"
	   (display-buffer-in-side-window)
	   (dedicated . t)
	   (window-height . 0.23)
	   (side . bottom)
	   (slot . -1)
	   )
	  )
	)

(defun emux/getBuffer()
      (interactive)
      (setq buff (read-buffer-to-switch "Raise window"))
      (display-buffer buff '(display-buffer-reuse-window . ())))

    (defun emux/go-to-window ()
      (interactive)
      (setq buff (read-buffer-to-switch "Go to window"))
      (select-window (get-buffer-window buff)))

    (defun emux/get-and-to-window ()
      (interactive)
      (setq buff (read-buffer-to-switch "Raise & Go to window"))
      (display-buffer buff '(display-buffer-reuse-window . ()))
      (select-window (get-buffer-window buff)))

    (defun emux/close-window ()
      (interactive)
      (setq buff (read-buffer-to-switch "Close window"))
      (delete-window (get-buffer-window buff)))

    (defun emux/close-help ()
      (interactive)
      (setq buff "*Help*") 
      (delete-window (get-buffer-window buff)))

    (defun emux/close-scratch ()
      (interactive)
      (setq buff "*scratch*") 
      (delete-window (get-buffer-window buff)))

    (setq vterm-open nil)
    (defun emux/toggle-close-open()
      (interactive)
;;      (setq buff "*vterm*")
      (if vterm-open (delete-window (get-buffer-window "*vterm*")) (vterm))
      (setq vterm-open (not vterm-open)))

(global-set-key (kbd "C-`") 'emux/toggle-close-open)

(straight-use-package 'denote)
;;  (setq default-directory "/Users/vamshi/notes")
  (setq denote-directory "/Users/vamshi/notes")

(org-timer-set-timer "1")

(global-set-key (kbd "M-/") 'hippie-expand)
(global-set-key (kbd "M-?") #'(lambda () (interactive) (hippie-expand -1)))
;; (global-set-key [remap dabbrev-expand] #'hippie-expand)

(straight-use-package 'pulsar)
(customize-set-variable
 'pulsar-pulse-functions
 '(other-window

 ))
(setq pulsar-face 'pulsar-magenta)
(setq pulsar-delay 0.055)
;; (define-key global-map (kbd  
(pulsar-global-mode t)
(global-hl-line-mode t)

(defun my-rustfmt ()
	 (interactive)
	 (save-buffer)
	 (async-shell-command (concat
			       "/nix/store/0k315psj935lvnrmq5l3lavrcywfwg7w-rust-default-1.69.0/bin/rustfmt "
			       (replace-regexp-in-string "/ssh:vamshi@192.168.64.3:" "" (buffer-file-name))))
	 (revert-buffer-quick)
	 )
    ;;  (defun key-my-rustfmt ()
    ;;    (interactive)
    ;;    (local-key-binding (kbd "C-x C-s") 'my-rustfmt))
    ;;  (add-hook 'rust-mode-hook 'key-my-rustfmt)
      ;; (setq string "/ssh:vamshi@192.168.64.3:/home/vamshi/cold-rust-haskell/practice/tuple.rs")
      ;; (setq new-string (replace-regexp-in-string "/ssh:vamshi@192.168.64.3:" "" string))
       ;;(message new-string)

       ;;/etc/profiles/per-user/vamshi/bin/rustfmt " (buffer-file-name)))
    (global-set-key (kbd "C-x C-s") 'save-buffer)
;;  #'(lambda () (interactive) (and (my-rustfmt) (save-buffer))))

(add-to-list 'load-path "~/mac-nix-darwin/emacs")
(require 'emux-keybindings)

(message (concat "Hello " user-full-name))

;; (require 'format-time-string)
(setq pkm-dir "~/pkm")
(defun insert-minutes ()
  "Inserts ** followed by the current minute."
  (interactive)
  (let ((minute (format-time-string "%M")))
    (insert (concat "** " minute) )
    (newline)
    (org-modal-insert-mode)
    )
  ) 

(defun insert-hours ()
  "Inserts ** followed by the current hour."
  (interactive)
  (let ((hour (format-time-string "%H")))
    (insert (concat "* " hour) )
    )
  )

(defun insert-hours-minutes ()
  "Inserts ** followed by the current hour."
  (interactive)
  (insert-hours)
  (newline)
  (insert-minutes)
  ) 
(setq cur-day (format-time-string "%d-%m-%Y"))
(defun open-file-today()
  (interactive)
  (let (
	(cur-file (concat  pkm-dir "/" cur-day ".org")))
    (if (file-exists-p cur-file)
	(find-file cur-file)
      (let ((description (read-string "Enter description:  ")))
	(find-file cur-file)
	(insert "#+Name: Notes")
	(newline)
	(insert "#+Description: " description)
	(newline)
	(newline)
	(insert-hours-minutes)
	)
      )
    )
  )
;; (define-auto-insert (directory . pkm-dir)

;;   )
