{
  description = "Vamshi's darwin system";

  inputs = {
    nixpkgs.url = "github:lnl7/nix-darwin";
# nixos/nixpkgs/nixpkgs-22.11-darwin";
    #nixpkgs-unstable"; # 
    emacs.url = "github:cmacrae/emacs";
    darwin.url = "github:lnl7/nix-darwin/master";
    darwin.inputs.nixpkgs.follows = "nixpkgs";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

  };

  outputs = { self,emacs, darwin, nixpkgs, home-manager }:
    let
      system = "aarch64-darwin";
      myPkgs = import nixpkgs {
        inherit system;
        #config = 
        config = {
          allowUnfree = true;
          # allowBroken = true;
          allowUnsupportedSystem = true;
        };
      };
    in {
      darwinConfigurations."cosmos" = darwin.lib.darwinSystem {
        system = "aarch64-darwin";
        modules = [
          ./darwin-configuration.nix
          {
            nix.binaryCaches = [ "https://cachix.org/api/v1/cache/emacs" ];

            nix.binaryCachePublicKeys = [
              "emacs.cachix.org-1:b1SMJNLY/mZF6GxQE+eDBeps7WnkT0Po55TAyzwOxTY="
            ];

            nixpkgs.overlays = [ emacs.overlay ];
          }
        ];
      };
      homeConfigurations."cosmos" = home-manager.lib.homeManagerConfiguration {
        #system = "aarch64-darwin";
        pkgs = myPkgs; # nixpkgs.legacyPackages."aarch64-darwin";
        modules = [
          ./nix
          ./code
          #	 ./emacs/file.nix
          #        ./hammerspoon
          #	        ./emacs
          #        ./fish
          ./git
          #  ./darwin-configuration.nix

          {
            # nixpkgs.overlays = with inputs; [
            #
            #   emacs-overlay.overlay
            #  nur.overlay
            #  kmonad.overlay
            # neovim-nightly-overlay.overlay
            #];

            programs.home-manager.enable = true;
            home = {
              username = "vamshi";
              homeDirectory = "/Users/vamshi";
              stateVersion = "22.11";
            };
          }

        ];
      };
    };
}
