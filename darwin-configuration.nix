{ config, pkgs, ... }:

{
  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [ vim ];
  #    nix.useDaemon = true;

  # Use a custom configuration.nix location.
  # $ darwin-rebuild switch -I darwin-config=$HOME/.config/nixpkgs/darwin/configuration.nix
  # environment.darwinConfig = "$HOME/.config/nixpkgs/darwin/configuration.nix";

  # Auto upgrade nix package and the daemon service.
  services.nix-daemon.enable = true;
  nix.package = pkgs.nix;
  services.emacs.enable = true;
  # Create /etc/zshrc that loads the nix-darwin environment.
  programs.zsh.enable = true; # default shell on catalina
  # programs.fish.enable = true;
  # { inputs, config, pkgs, ... }: {
  /* homebrew = {
         enable = true;
         onActivation.autoUpdate = false;
         onActivation.upgrade = true;
         global = {
           brewfile = true;
           #      noLock = true;
         };
        taps = [
     #    "d12frosted/emacs-plus"
     ];
         brews = [
     #"emacs"
     #            "emacs-plus@29"
           #      "amethyst"
         ];
         casks = [ "amethyst" "hammerspoon" ];
       };
  */
  #  programs.git.enable = true;

  #  services.amethyst.enable  = true;

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 4;
}
