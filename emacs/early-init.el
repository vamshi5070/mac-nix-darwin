(add-to-list 'default-frame-alist '(undecorated . t))

(if (fboundp 'tool-bar-mode)
    (tool-bar-mode   -1))

(if (fboundp 'scroll-bar-mode)
    (scroll-bar-mode -1))

;; (menu-bar-mode   -1)
;; (electric-indent-mode -1)
(fset 'yes-or-no-p 'y-or-n-p)
(provide 'early-init)
